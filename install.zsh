#!/bin/zsh

# Sources :
# https://github.com/drduh/macOS-Security-and-Privacy-Guide
# https://github.com/SummitRoute/osxlockdown/blob/master/commands.yaml
# https://github.com/joeyhoer/starter/blob/master/system

echo "This package will set useful parameters to ensure that your mac will stay protected"
echo "It might take several minutes, please let the script work until the end!"

if ! sudo -n true 2>/dev/null; then
	echo "Please enter your Mac password when requested !"
fi

# Close system preferences before starting
if [[ $(ps ax | grep -c '/Applications/System Preferences.app/Contents/MacOS/System Preferences') -eq 2 ]] ; then
    killall "System Preferences"
    sleep 1
fi

###############################################################################
# System updates
###############################################################################

# Check updates
LASTUPDATE=$(defaults read /Library/Preferences/com.apple.SoftwareUpdate | grep LastSuccessfulDate | sed -e 's@^.* "\([0-9\\-]*\) .*$@\1@')
if [[ "${LASTUPDATE}" != "$(date +%Y-%m-%d)" ]] ; then
    echo "Checking system updates" ;
    sudo softwareupdate -i -a
fi

# Set auto updates to "every day" ("once a week" by default)
sudo softwareupdate --schedule on >/dev/null 2>&1
sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate ScheduleFrequency -int 1

# Auto check all updates (1) in background (2), install them (3, 4) and security (5, 6)
sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate AutomaticCheckEnabled -bool true
sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate AutomaticDownload -bool true
sudo defaults write /Library/Preferences/com.apple.commerce AutoUpdate -bool true
sudo defaults write /Library/Preferences/com.apple.commerce AutoUpdateRestartRequired -bool true
sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate ConfigDataInstall -bool true
sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate CriticalUpdateInstall -bool true

###############################################################################
# System security
###############################################################################

echo "Updating system security settings..."
# Enable Gatekeeper
spctl --status | grep "assessments enabled" >/dev/null 2>&1
if [[ $? -ne 0 ]]; then
    sudo spctl --master-enable
fi

# Require an administrator password to access system-wide preferences
if [[ -n "$(security authorizationdb read system.preferences 2> /dev/null | grep -A1 shared | grep -E '(true|false)' | grep 'false')" ]]; then
    security authorizationdb read system.preferences > /tmp/system.preferences.plist
    /usr/libexec/PlistBuddy -c "Set :shared false" /tmp/system.preferences.plist
    security authorizationdb write system.preferences < /tmp/system.preferences.plist
fi

# Activate TouchID
if [[ $(bioutil -r -s | grep "Touch ID for unlock" |awk -F': ' '{print $2}') -ne 1 ]] ; then
    echo "Activate TouchID"
    bioutil -w -u 1
fi

# Add TouchID support to sudo Term
if ! grep -q pam_tid.so /etc/pam.d/sudo ; then
    echo "Add TouchID to sudo"
	sudo cp /etc/pam.d/sudo /etc/pam.d/sudo.bak
	sudo chmod 777 /etc/pam.d/sudo
    sudo sed -i '' -e "2s/^//p; 2s/^.*/auth       sufficient     pam_tid.so/" /etc/pam.d/sudo
	sudo chmod 444 /etc/pam.d/sudo
fi

# Remove Guest user
# sudo fdesetup remove -user Guest
sudo defaults write /Library/Preferences/com.apple.AppleFileServer guestAccess -bool NO
sudo defaults write /Library/Preferences/SystemConfiguration/com.apple.smb.server AllowGuestAccess -bool NO
sudo defaults write /Library/Preferences/com.apple.loginwindow GuestEnabled -bool NO

###############################################################################
# Network and Firewall
###############################################################################

echo "Updating network security settings..."

# Disable Wake on Network Access
sudo systemsetup getwakeonnetworkaccess | grep "Wake On Network Access: Off" >/dev/null 2>&1
if [[ $? -ne 0 ]]; then
    sudo systemsetup -setwakeonnetworkaccess off
fi

# Setup firewall
if [[ "$(defaults read /Library/Preferences/com.apple.alf globalstate)" -ne 1 ]] ; then
  echo "Enabling [Firewall].."
  sudo defaults write /Library/Preferences/com.apple.alf globalstate -int 1
fi

# Enable Firewall Stealth mode
/usr/libexec/ApplicationFirewall/socketfilterfw --getstealthmode | grep "Stealth mode enabled"  >/dev/null 2>&1
if [[ $? -ne 0 ]]; then
    sudo /usr/libexec/ApplicationFirewall/socketfilterfw --setstealthmode on
fi

# Disable signed apps from being auto-permitted to listen through firewall
sudo defaults write /Library/Preferences/com.apple.alf allowsignedenabled -bool false

# Disable Captive portal
# defaults read /Library/Preferences/SystemConfiguration/com.apple.captive.control.plist >/dev/null 2>&1
# if [[ $? -ne 0 ]]; then
# 	sudo defaults write /Library/Preferences/SystemConfiguration/com.apple.captive.control Active -boolean false
# fi
# To come back
# sudo defaults delete /Library/Preferences/SystemConfiguration/com.apple.captive.control Active

# Disable ipv6
networksetup -listallnetworkservices | \
    while read i; do
        SUPPORT=$(networksetup -getinfo "$i" | grep "IPv6: Automatic") && \
            if [[ -n "$SUPPORT" ]]; then
                networksetup -setv6off "$i";
            fi;
    done;

###############################################################################
# Screen Saver and screen lock
###############################################################################

echo "Updating user security settings..."

# Securing screensaver : "Set an inactivity interval of less than 5 minutes for the screen saver"
UUID=$(ioreg -rd1 -c IOPlatformExpertDevice | grep "IOPlatformUUID" | sed -e 's/^.*"\(.*\)"$/\1/')
PREF=$HOME/Library/Preferences/ByHost/com.apple.screensaver.${UUID}
if [[ -e ${PREF}.plist ]]; then
    defaults -currentHost write ${PREF}.plist idleTime -int 300;
fi;

# Require a password to wake the computer from sleep or screen saver
defaults write com.apple.screensaver askForPassword -bool true

# Ensure screen locks immediately when requested
defaults write com.apple.screensaver askForPasswordDelay -int 0


###############################################################################
# Disable unnecessary Services
###############################################################################

echo "Updating services security settings..."

# Disable Remote Apple Events
sudo systemsetup -getremoteappleevents | grep "Remote Apple Events: Off" >/dev/null 2>&1
if [[ $? -ne 0 ]]; then
    sudo systemsetup -setremoteappleevents off
fi

# Disable Remote Login
sudo systemsetup -getremotelogin | grep "Remote Login: Off" >/dev/null 2>&1
if [[ $? -ne 0 ]]; then
    sudo systemsetup -f -setremotelogin off
fi

# Disable Remote Management
if [[ -n "$(ps -ef | egrep "/System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/MacOS/[A]RDAgent")" ]]; then
    sudo /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -deactivate -stop
fi

# Disable file sharing
if [[ -n "$(launchctl list | egrep AppleFileServer)" ]]; then
    if [[ -n "$(grep -i array /Library/Preferences/SystemConfiguration/com.apple.smb.server.plist)" ]] ; then
        launchctl unload -w /System/Library/LaunchDaemons/com.apple.AppleFileServer.plist
        launchctl unload -w /System/Library/LaunchDaemons/com.apple.smbd.plist
    fi
fi

# Turn off Siri from learning from Apple Mail.
defaults write com.apple.suggestions SiriCanLearnFromAppBlacklist -array com.apple.mail

# Prevent auto open in Safari
defaults write com.apple.Safari AutoOpenSafeDownloads -bool false

# Disable Screen Sharing - does not work
#if [[ -e /System/Library/LaunchDaemons/com.apple.screensharing.plist ]]; then
#    STATUS=$(launchctl load /System/Library/LaunchDaemons/com.apple.screensharing.plist | grep -v "Service is disabled")
#    if [[ -n "$STATUS" ]]; then
#        launchctl unload -w /System/Library/LaunchDaemons/com.apple.screensharing.plist
#    fi
#fi

# Disable internet sharing -- not tested
if [[ -e /Library/Preferences/SystemConfiguration/com.apple.nat ]]; then
    defaults write /Library/Preferences/SystemConfiguration/com.apple.nat NAT -dict-add Enabled -int 0
fi

# Disable Printer sharing
if [[ -n "$(system_profiler SPPrintersDataType | grep Shared | grep Yes)" ]]; then
    cupsctl --no-share-printers
fi

###############################################################################
# Install Homebrew and security services
###############################################################################

echo "Adding security stuff..."

# Install Homebrew if needed
if [[ ! -f /usr/local/bin/brew ]] ; then
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" < /dev/null
fi

brew upgrade
brew upgrade --cask

# Install Patrick Wardle security tools
# oversight removed -- memory leak
declare -a sec_form=("ransomwhere" "lulu" "knockknock" "taskexplorer")

for formula in "${sec_form[@]}" ; do
    brew cask list ${formula} &>/dev/null || brew cask install ${formula}
done

# Security from Howard Oakley
# SilentKnight: fully automatic security checks
# Install from https://eclecticlight.co/downloads/
# May be available with brew later https://github.com/sticklerm3/homebrew-pourhouse
brew cask install https://raw.githubusercontent.com/Sticklerm3/homebrew-pourhouse/master/Casks/silnite.rb
silnite a 


# Google updates every day
defaults write com.google.Keystone.Agent checkInterval 86400


# Setup filevault in next login
 if [[ $(fdesetup isactive) == "false" ]] ; then
    echo "Enabling FileVault.."
    sudo fdesetup enable -user ${USER} -defer -forceatlogin 3 -dontaskatlogout
 fi

# Test if smartcard can be used for that
# https://derflounder.wordpress.com/2014/08/13/filevault-2-institutional-recovery-keys-creation-deployment-and-use/

echo "You're all set."
echo "Please help us to keep your Mac secure and DO NOT install Software you don't trust."
echo "Ask us if you're not sure."
