# Hardened

Set of rules to: 
- enable or configure recommended security features
- disable useless OS X features that may increase your Mac's attack surface. 

Target computers are Macbooks with TouchID used for day to day work. 
I tried to keep a good balance between security and usability.

Features included:
- Setup automatic daily system updates
- Verify that Gatekeeper is enabled
- Require an administrator password to access system-wide preferences
- Activate TouchID and add TouchID to sudo commands
- Disable Guest user
- Disable Wake on Network Access
- Activate firewall and Stealth mode
- Disable signed apps from being auto-permitted to listen through firewall
- Disable Captive portal
- Disable IPv6
- Securing screensaver if used
- Activate auto-screen lock with password
- Disable Remote Logging and Remote Management
- Disable Remote Apple Events, File, Screen and Printer Sharing
- Install Homebrew and Ransomwhere
- Enable FileVault

Features untouched:
- Current user account is still an administrator account
- AirDrop as it's great for peer to peer sharing files between computers
- Bluetooth ad it is used for external mouse, airpods or AirDrop
- Safari as it is the most optimized browser for reducing battery use
- iCloud 

Features that I would like to add:
- Verify that user password is strong enough
- Set AirDrop visibility to "Contacts" only
- Disable screen sharing