
This page gathers useful commands to protect your mac.

Last updated with `macOS X Big Sur`.

I found inspiration on the following sites:
  - [macOS Security and Privacy Guide](ttps://github.com/drduh/macOS-Security-and-Privacy-Guide)
  - [(old) SummitRoute settings](https://github.com/SummitRoute/osxlockdown/blob/master/commands.yaml)
  - [(old) joeyhoer settings](https://github.com/joeyhoer/starter/blob/master/system)

# Foreplay

Close System preferences before starting to change settings
```
if [[ $(ps ax | grep -c '/Applications/System Preferences.app/Contents/MacOS/System Preferences') -eq 2 ]] ; then
    killall "System Preferences"
    sleep 1
fi
```

# Activate System updates

Your system need to be up to date. This is the way.
```
LASTUPDATE=$(defaults read /Library/Preferences/com.apple.SoftwareUpdate | grep LastSuccessfulDate | sed -e 's@^.* "\([0-9\\-]*\) .*$@\1@')
if [[ "${LASTUPDATE}" != "$(date +%Y-%m-%d)" ]] ; then
    echo "Outdated. Checking system updates" ;
    sudo softwareupdate -i -a
else
    echo "Good to go. Last updated: $LASTUPDATE"
fi
```

Set auto updates to "every day" instead of "once a week"
```
sudo softwareupdate --schedule on >/dev/null 2>&1
sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate ScheduleFrequency -int 1
```

Check all available updates in background
```
sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate AutomaticCheckEnabled -bool true
sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate AutomaticDownload -bool true
```

Auto install updates
```
sudo defaults write /Library/Preferences/com.apple.commerce AutoUpdate -bool true
sudo defaults write /Library/Preferences/com.apple.commerce AutoUpdateRestartRequired -bool true
sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate ConfigDataInstall -bool true
sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate CriticalUpdateInstall -bool true
```

# Add basic security settings

Enable Gatekeeper
```
spctl --status | grep "assessments enabled" >/dev/null 2>&1
if [[ $? -ne 0 ]]; then
    sudo spctl --master-enable
fi
```

Require an administrator password to access system-wide preferences
```
if [[ -n "$(security authorizationdb read system.preferences 2> /dev/null | grep -A1 shared | grep -E '(true|false)' | grep 'false')" ]]; then
    security authorizationdb read system.preferences > /tmp/system.preferences.plist
    /usr/libexec/PlistBuddy -c "Set :shared false" /tmp/system.preferences.plist
    security authorizationdb write system.preferences < /tmp/system.preferences.plist
fi
```

Activate TouchID
```
if [[ $(bioutil -r -s | grep "Touch ID for unlock" |awk -F': ' '{print $2}') -ne 1 ]] ; then
    echo "Activate TouchID"
    bioutil -w -u 1
fi
```

Add TouchID support to Terminal for `sudo`
```
if ! grep -q pam_tid.so /etc/pam.d/sudo ; then
    echo "Add TouchID to sudo"
        sudo cp /etc/pam.d/sudo /etc/pam.d/sudo.bak
        sudo chmod 777 /etc/pam.d/sudo
    sudo sed -i '' -e "2s/^//p; 2s/^.*/auth       sufficient     pam_tid.so/" /etc/pam.d/sudo
        sudo chmod 444 /etc/pam.d/sudo
fi
```

Remove Guest user access
```
sudo defaults write /Library/Preferences/com.apple.AppleFileServer guestAccess -bool false
sudo defaults write /Library/Preferences/SystemConfiguration/com.apple.smb.server AllowGuestAccess -bool false
sudo defaults write /Library/Preferences/com.apple.loginwindow GuestEnabled -bool false
```

Enforce filevault (disk encryption) in next login (not tested as I setup Filevault during install)
```
 if [[ $(fdesetup isactive) == "false" ]] ; then
    echo "Enabling FileVault.."
    sudo fdesetup enable -user ${USER} -defer -forceatlogin 3 -dontaskatlogout
 fi
 ```

Disable crash reporter
```
defaults write com.apple.CrashReporter DialogType none
```

Show filename extensions
```
defaults write NSGlobalDomain AppleShowAllExtensions -bool true
```

# Network and Firewall

Disable "Wake on Network"
```
sudo systemsetup getwakeonnetworkaccess | grep "Wake On Network Access: Off" >/dev/null 2>&1
if [[ $? -ne 0 ]]; then
    sudo systemsetup -setwakeonnetworkaccess off
fi
```

Enable build-in firewall
```
if [[ "$(defaults read /Library/Preferences/com.apple.alf globalstate)" -ne 1 ]] ; then
  echo "Enabling [Firewall].."
  sudo defaults write /Library/Preferences/com.apple.alf globalstate -int 1
fi
```

Enable Stealth mode in this Firewall
```
/usr/libexec/ApplicationFirewall/socketfilterfw --getstealthmode | grep "Stealth mode enabled"  >/dev/null 2>&1
if [[ $? -ne 0 ]]; then
    sudo /usr/libexec/ApplicationFirewall/socketfilterfw --setstealthmode on
fi
```

Disable signed apps from being auto-permitted to listen through firewall
```
sudo defaults write /Library/Preferences/com.apple.alf allowsignedenabled -bool false
```

Disable IPV6 if not needed
```
networksetup -listallnetworkservices | \
    while read i; do
        SUPPORT=$(networksetup -getinfo "$i" | grep "IPv6: Automatic") && \
            if [[ -n "$SUPPORT" ]]; then
                networksetup -setv6off "$i";
            fi;
    done;
```

# Screen Saver and screen lock

Set an inactivity interval of less than 5 minutes for the screen saver
```
UUID=$(ioreg -rd1 -c IOPlatformExpertDevice | grep "IOPlatformUUID" | sed -e 's/^.*"\(.*\)"$/\1/')
PREF=$HOME/Library/Preferences/ByHost/com.apple.screensaver.${UUID}
if [[ -e ${PREF}.plist ]]; then
    defaults -currentHost write ${PREF}.plist idleTime -int 300;
fi;
```

Require a password to wake the computer from sleep or screen saver
```
defaults write com.apple.screensaver askForPassword -bool true
```

Ensure screen locks immediately when requested
```
defaults write com.apple.screensaver askForPasswordDelay -int 0
```

Add a `lock button` in touchbar, just near Touch ID. It's very convenient to lock the session before leaving the mac. 
In this example, I add 3 other functions before: brightness control, music play/pause and volume control.

```
defaults write ~/Library/Preferences/com.apple.controlstrip MiniCustomized '("com.apple.system.brightness", "com.apple.system.media-play-pause", "com.apple.system.volume", "com.apple.system.screen-lock")'

killall "ControlStrip";
```
[Source](https://blog.eriknicolasgomez.com/2016/11/28/managing-or-setting-the-mini-touchbar-control-strip/)

# Disable useless services

Disable Remote Apple Events
```
sudo systemsetup -getremoteappleevents | grep "Remote Apple Events: Off" >/dev/null 2>&1
if [[ $? -ne 0 ]]; then
    sudo systemsetup -setremoteappleevents off
fi
```

Disable Remote Login
```
sudo systemsetup -getremotelogin | grep "Remote Login: Off" >/dev/null 2>&1
if [[ $? -ne 0 ]]; then
    sudo systemsetup -f -setremotelogin off
fi
```

Disable Remote Management
```
if [[ -n "$(ps -ef | egrep "/System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/MacOS/[A]RDAgent")" ]]; then
    sudo /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -deactivate -stop
fi
```

Disable file sharing
```
if [[ -n "$(launchctl list | egrep AppleFileServer)" ]]; then
    if [[ -n "$(grep -i array /Library/Preferences/SystemConfiguration/com.apple.smb.server.plist)" ]] ; then
        launchctl unload -w /System/Library/LaunchDaemons/com.apple.AppleFileServer.plist
        launchctl unload -w /System/Library/LaunchDaemons/com.apple.smbd.plist
    fi
fi
```

Disable Printer sharing
```
if [[ -n "$(system_profiler SPPrintersDataType | grep Shared | grep Yes)" ]]; then
    cupsctl --no-share-printers
fi
```

Disable internet sharing -- not tested
```
if [[ -e /Library/Preferences/SystemConfiguration/com.apple.nat ]]; then
    defaults write /Library/Preferences/SystemConfiguration/com.apple.nat NAT -dict-add Enabled -int 0
fi
```

# Install Homebrew and add security tools

Install Homebrew if needed and upgrade everything
```
if [[ ! -f /usr/local/bin/brew ]] ; then
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" < /dev/null
fi

brew upgrade
brew upgrade --cask
```

Install [Security tools by Patrick Wardle](https://objective-see.com/products.html):
  - `Ransomwhere` stop any suspicious encryption processes 
  - `Lulu` is a user friendly firewall (as Little Snitch)
  - `Knockknock` lists persistently installed software
  - I removed `oversight` due to an unresolved memory leak. But feel free to add it to the list.
```
declare -a sec_form=("ransomwhere" "lulu" "knockknock")

for formula in "${sec_form[@]}" ; do
    brew cask list ${formula} &>/dev/null || brew cask install ${formula}
done
```

Install [SilentKnight by Howard Oakley](https://eclecticlight.co/downloads/) for fully automatic security checks. Maybe available via [brew pour house later](https://github.com/sticklerm3/homebrew-pourhouse)
```
brew cask install https://raw.githubusercontent.com/Sticklerm3/homebrew-pourhouse/master/Casks/silnite.rb
silnite a
```

# Application settings

Turn off Siri from learning from Apple Mail.
```
defaults write com.apple.suggestions SiriCanLearnFromAppBlacklist -array com.apple.mail
```

Prevent auto open in Safari
```
defaults write com.apple.Safari AutoOpenSafeDownloads -bool false
```

Update google applications (ie Chrome) every day:
```
defaults write com.google.Keystone.Agent checkInterval 86400
```

Stop opening Photo when you plug your iPhone
```
defaults -currentHost write com.apple.ImageCapture disableHotPlug -bool true
```

Keep Desktop clean with screenshot in 
  - Downloads directory
  - Prefixed with Shot
  - in JPG and not PNG
  - Silent mode
```
defaults write com.apple.screencapture location /Users/$USER/Downloads/
defaults write com.apple.screencapture name "Shot"
defaults write com.apple.screencapture type jpg
defaults write com.apple.screencapture show-thumbnail -bool false
killall SystemUIServer
```

Remove annoying auto-correction
```
defaults write NSGlobalDomain NSAutomaticSpellingCorrectionEnabled -bool false
```

# TODO and to DISCUSS

- Disable spotlight localization/suggestions/indexation ?
- Disable captive portal ?
```
defaults read /Library/Preferences/SystemConfiguration/com.apple.captive.control.plist >/dev/null 2>&1
# if [[ $? -ne 0 ]]; then
#       sudo defaults write /Library/Preferences/SystemConfiguration/com.apple.captive.control Active -boolean false
# fi
# To come back
# sudo defaults delete /Library/Preferences/SystemConfiguration/com.apple.captive.control Active
```
- Set a Firmware password ?
- Remove Apple Remote Desktop Settings?
- 
- Safari parameters: logging/cookies
- [Auto update Homebrew with launchd](https://gist.github.com/jb510/99f12b1ac70f1cf8b780)
- Secure DNS ?
- Auto spoof mac address ?
```
sudo ifconfig en0 ether $(openssl rand -hex 6 | sed 's%\(..\)%\1:%g; s%.$%%')
```
- What about using [Santa](https://github.com/google/santa/)

- What about a second password to auto-erase disk ?